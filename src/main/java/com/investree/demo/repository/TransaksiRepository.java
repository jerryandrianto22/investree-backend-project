package com.investree.demo.repository;

import com.investree.demo.model.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TransaksiRepository extends JpaRepository<Transaksi, Long> {
    @Query("SELECT c FROM Transaksi c")
    public List<Transaksi> getList();
}
