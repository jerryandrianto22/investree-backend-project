package com.investree.demo.repository;

import com.investree.demo.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    @Query("SELECT c FROM User c")
    public List<User> getList();

    @Query("select c from User c WHERE c.id = :id")
    public User getByID(@Param("id") Long id);
}
