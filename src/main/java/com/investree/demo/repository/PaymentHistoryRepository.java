package com.investree.demo.repository;

import com.investree.demo.model.PaymentHistory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PaymentHistoryRepository extends PagingAndSortingRepository<PaymentHistory, Long> {
    @Query("SELECT c FROM PaymentHistory c")
    public List<PaymentHistory> getList();
}
